﻿namespace QuickTest.Src
{
    public static class QuickTest
    {
        public static bool OutParameter(out int x, out int y)
        {
            x = 45;
            y = 123;
            return x == y;
        }
    }
}
