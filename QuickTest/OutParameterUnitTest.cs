﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace QuickTest.UnitTestProject
{
    using Src;

    [TestClass]
    public class OutParameterUnitTest
    {
        [TestMethod]
        public void SameValue()
        {
            int x = 45;
            var y = 45;
            bool expected = true;

            bool actual = QuickTest.OutParameter(out x, out y);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithOutValue()
        {
            var expected = false;
            int x;
            int y;

            var actual = QuickTest.OutParameter(out x, out y);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DifferentValue()
        {
            int x = 100;
            var y = 1000;
            bool expected = false;

            bool actual = QuickTest.OutParameter(out x, out y);

            Assert.AreEqual(expected, actual);
        }
    }
}
